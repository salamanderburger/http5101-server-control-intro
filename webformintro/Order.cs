﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformintro
{
    public class Order
    {
        // an order consists of the following pieces of information
        // customer
        // pizza
        // delivery information

        public Pizza pizza;
        public Customer customer;
        public Delivery delivery;

        public Order(Pizza p, Customer c, Delivery d)
        {
            pizza = p;
            customer = c;
            delivery = d;

        }

        public string PrintReceipt()
        {
            string receipt = "Order Receipt:<br>";
            receipt += "Your total is :"+CalculateOrder().ToString()+"<br/>";
            receipt += "Name: "+customer.CustomerName+"<br/>";
            receipt += "Email: " + customer.CustomerEmail + "<br/>";
            receipt += "Phone Number: " + customer.CustomerPhone + "<br/>";

            receipt += "Pizza Size: "+ pizza.size+"<br/>";
            receipt += "Cut into " + pizza.slices.ToString() + "<br/>";
            receipt += "Toppings: " + String.Join(" ", pizza.toppings.ToArray()) + "<br/>";

            receipt += "Delivery to : " + delivery.address + " at " + delivery.time.ToString();
            
            return receipt;
        }

        public double CalculateOrder()
        {
            double total = 0;
            if (pizza.size=="S")
            {
                total = 5;
            }else if(pizza.size=="M")
            {
                total = 7;
            }else if(pizza.size=="L")
            {
                total = 10;
            }

            total += pizza.toppings.Count()*0.50;

            return total;
        }

    }
}