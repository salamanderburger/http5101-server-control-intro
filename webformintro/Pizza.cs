﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformintro
{
    public class Pizza
    {
        //information that we want to store in the Pizza Object
        //Size
        //toppings

        // A List<string> is a list of strings. A string is going to be our topping (such as "pepperoni", "cheese")
        public List<string> toppings;

        // size is the size of the pizza. It should be S, M, or L.
        public string size;

        // slices is the number of slices that our pizza is going to be cut into.
        public int slices;

        
        //This part is a constructor function. It takes parameters, which are going to end up being the values we
        //extract from 
        public Pizza(List<string> t, string si, int sli)
        {
            toppings = t;
            size = si;
            slices = sli;
        }
    }
}