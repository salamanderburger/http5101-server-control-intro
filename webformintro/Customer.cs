﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformintro
{
    public class Customer
    {
        //Information in the customer that we want to store in the object
        //customername, 
        //customerphone,
        //customeremail

        private string customerName;
        private string customerPhone;
        private string customerEmail;

        public Customer()
        {

        }
        //property accessors
        //shorthand {get;set;}
        //Must start with Capitals
        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        public string CustomerPhone
        {
            get { return customerPhone; }
            set { customerPhone = value; }
        }
        public string CustomerEmail
        {
            get { return customerEmail; }
            set { customerEmail = value; }
        }

    }
}