﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webformintro
{
    public partial class testwebform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //This is where you'll be doing stuff for your lab
            mytestbox.InnerHtml = "I successfully did server side processing!!";
        }
    }
}