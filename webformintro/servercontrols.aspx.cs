﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webformintro
{
    public partial class servercontrols : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            int employees = 13;
            int managers = 5;
            int staff = employees + managers;

            string employeestring = employees.ToString(); 

            string slogan = "Best Pizza In Town!";
            DateTime established = DateTime.ParseExact("1992/12/25", "yyyy/MM/dd", CultureInfo.InvariantCulture);
            string tagline = "since "+ established.ToString("yyyy");


            //List<int> prices = new List<int> { 1, 2, 3};
            List<string> storemenu = new List<string> { "Chicken Wings", "Fries", "Pudding", "Dipping Sauce" };
            //string s_storemenu = "Chicken wings, fries, pudding, dipping sauce";

            info.InnerHtml = slogan;
            info.InnerHtml += " -- We mean it!<br>";
            footer.InnerHtml = tagline;

            //Example of an if statement.
            //staff: 18
            // managers: 5
            
            if (staff < 20 && managers <= 5)
            {
                subheading.InnerHtml += "<br>We're Hiring!<br>";
            }

            info.InnerHtml += "<br>";


            // List < string > storemenu = new List<string> { "Chicken Wings", "Fries", "Pudding", "Dipping Sauce" };
            foreach (string menuitem in storemenu)
            {
                info.InnerHtml += menuitem+" # ";
            }
            int j = 0;
           



        }

        protected void OrderChoice_Validator(object source, ServerValidateEventArgs args)
        {
            if(orderType.SelectedValue=="Delivery")
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Order(object sender, EventArgs e)
        {

            //first check to make sure everything is valid
            if (!Page.IsValid)
            {
                return;
            }
            info.InnerHtml = "Thank you for your order!";

            /*
             * Tasks: Initialize our three objects: Pizza, Customer, Delivery 
             * 
            */

            /*
             * Creating the pizza object
             * 1) Get the appropriate information from our .aspx file
             * 2) Set the datatype to the same datatype as it is set up in our .cs file
             * 3) use our aspx.cs file to create an object and set the values.
            */
            string size = pizzaSize.SelectedItem.Value.ToString();
            int slices = int.Parse(pizzaSlices.Text);
            List<String> toppings = new List<String>{"Cheese", "Tomato Sauce"};
            Pizza newpizza = new Pizza(toppings,size,slices);

            /*
             * Creating the clients object
             * Same process as getting the pizza object. Collect variables and set the values.
             * Instead of creating our object with the values in them, we set the values later with a property accessor
            */

            string name = clientName.Text.ToString();
            string email = clientEmail.Text.ToString();
            string phone = clientPhone.Text.ToString();
            Customer newcustomer = new Customer();
            newcustomer.CustomerName = name;
            newcustomer.CustomerPhone = phone;
            newcustomer.CustomerEmail = email;

            /*
             * 
             * To successfully get the date, we are going to pull the current date from DateTime and add 24 hour format.
             * 
            */
            string address = deliveryAddr.Text.ToString();
            string today = DateTime.Today.ToString("dd-MM-yyyy");
            string time = deliveryTime.Text.ToString();
            string hour = time.Substring(0,2); //should always be the hour (first two characters)
            string minute = time.Substring(time.Length-2); //should always be 00 (last two characters)
            string trydate = today + " " + hour + ":" + minute;
            DateTime deliverytime = DateTime.ParseExact(trydate, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);

            Delivery newdelivery = new Delivery(address, deliverytime);

            //add the extra toppings to the pizza..

            List<string> pizzatoppings = new List<string>(); 

            foreach (Control control in toppings_container.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox topping = (CheckBox)control;
                    if (topping.Checked)
                    {
                        pizzatoppings.Add(topping.Text);
                    }

                }
            }
            newpizza.toppings = newpizza.toppings.Concat(pizzatoppings).ToList();

            Order neworder = new Order(newpizza, newcustomer, newdelivery);
            /*
            OrderRes.InnerHtml = neworder.pizza.slices.ToString();
            OrderRes.InnerHtml += neworder.customer.CustomerName;
            OrderRes.InnerHtml += neworder.delivery.address;
            */
            OrderRes.InnerHtml = neworder.PrintReceipt();


            //reference newpizza again, adding the previous toppings
            /*
            

            PizzaRes.InnerHtml = "The pizza toppings are: "+String.Join(" ",newpizza.toppings.ToArray());

            ClientRes.InnerHtml = "The name under the order is :" + newcustomer.CustomerName;

            DeliveryRes.InnerHtml = "The time for the delivery is: " + newdelivery.time.ToString();
            */
            
        }
    }
}