﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrols.aspx.cs" Inherits="webformintro.servercontrols" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pizza Order</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Pizza Order</h2>
            <h3 id="subheading" runat="server"></h3>
            <div id="info" runat="server">

            </div>
            <label>First Name:</label><br />
            <asp:TextBox runat="server" ID="clientName"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <label>Phone Number:</label><br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <label>Email:</label><br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <label>Address:</label><br />
            <asp:TextBox runat="server" ID="deliveryAddr" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="deliveryAddr" ErrorMessage="Please enter an address"></asp:RequiredFieldValidator>
            <br />
            <label>Delivery Time:</label><br />
            <asp:DropDownList runat="server" ID="deliveryTime" placeholder="Delivery Time (24 hour format)">
                <asp:ListItem Text="11:00am" Value="1100"></asp:ListItem>
                <asp:ListItem Text="12:00pm" Value="1200"></asp:ListItem>
                <asp:ListItem Text="1:00pm" Value="1300"></asp:ListItem>
                <asp:ListItem Text="2:00pm" Value="1400"></asp:ListItem>
                <asp:ListItem Text="3:00pm" Value="1500"></asp:ListItem>
                <asp:ListItem Text="4:00pm" Value="1600"></asp:ListItem>
                <asp:ListItem Text="5:00pm" Value="1700"></asp:ListItem>
                <asp:ListItem Text="6:00pm" Value="1800"></asp:ListItem>
                <asp:ListItem Text="7:00pm" Value="1900"></asp:ListItem>
                <asp:ListItem Text="8:00pm" Value="2000"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <label>What size?</label><br />
            <asp:DropDownList runat="server" ID="pizzaSize">
                <asp:ListItem Value="S" Text="Small"></asp:ListItem>
                <asp:ListItem Value="M" Text="Medium"></asp:ListItem>
                <asp:ListItem Value="L" Text="Large"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <label>How many slices?</label><br />
            <asp:TextBox runat="server" ID="pizzaSlices" placeholder="Number of Slices"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="pizzaSlices" Type="Integer" MinimumValue="2" MaximumValue="12" ErrorMessage="Enter a valid number of slices (between 2 and 12)"></asp:RangeValidator>
            <br />
            <label>Choose One:</label><br />
            <asp:RadioButtonList runat="server" ID="orderType">
                <asp:ListItem Text="Delivery" >Delivery</asp:ListItem>
                <asp:ListItem Text="Pick-Up" >Pick-Up</asp:ListItem>
            </asp:RadioButtonList>
            <asp:CustomValidator runat="server" ErrorMessage="Deliveries Only! Sorry!" ControlToValidate="orderType" OnServerValidate="OrderChoice_Validator"></asp:CustomValidator>
            <br />
            <label>Toppings:</label><br />
            <div id="toppings_container" runat="server">
                <asp:CheckBox runat="server" ID="pizzaTopping1" Text="Mushrooms" />
                <asp:CheckBox runat="server" ID="pizzaTopping2" Text="Pineapple" />
                <asp:CheckBox runat="server" ID="pizzaTopping3" Text="Pepperoni" />
                <asp:CheckBox runat="server" ID="pizzaTopping4" Text="Olives" />
                <asp:CheckBox runat="server" ID="pizzaTopping5" Text="Bacon" />
                <asp:CheckBox runat="server" ID="pizzaTopping6" Text="Tomatoes" />
                <asp:CheckBox runat="server" ID="pizzaTopping7" Text="Chicken" />
                <asp:CheckBox runat="server" ID="pizzaTopping8" Text="Green Peppers" />
                <asp:CheckBox runat="server" ID="pizzaTopping9" Text="Banana Peppers" />
            </div>
            <br />
            <asp:Button runat="server" ID="myButton" OnClick="Order" Text="Submit"/>
            <br />
            <div runat="server" id="PizzaRes">


            </div>
            <div runat="server" id="ClientRes">

            </div>
            <div runat="server" id="DeliveryRes">

            </div>
            <div runat="server" id="OrderRes">

            </div>

            <footer runat="server" id="footer">


            </footer>
        </div>
    </form>
</body>
</html>
