﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformintro
{
    public class Delivery
    {
        //information that we want to store in the delivery object
        //wether the order is a pick up or delivery
        //delivery address
        //delivery time

        public string address;
        public DateTime time;
        public Delivery(string a, DateTime t)
        {
            address = a;
            time = t;
        }
    }
}